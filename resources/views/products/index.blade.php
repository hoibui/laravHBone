@extends('products.master')
@section('content')

<div class="row">
	<div class="col-lg-12">
		<div>PRODUCTS</div>
		
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="pull-right">
			<a class="btn btn-xs btn-success" href="{{route('products.create')}}">Add New Product</a>
		</div>
	</div>
</div>
<br>
<div class="row">
		<table class="table table-bordered">
			<tr>
				<th>No.</th>
				<th>ID</th>
				<th>Name</th>
				<th>Price</th>
			</tr>
			@foreach($products as $product)
			<tr>
				<td>{{++$i}}</td>
				<td>{{$product->id}}</td>
				<td>{{$product->name}}</td> 
				<td>{{$product->price}}</td>
				<td>
					<a class="btn btn-xs btn-success" href="{{route('products.show',$product->id)}}">Show</a>
					<a class="btn btn-xs btn-primary" href="{{route('products.edit',$product->id)}}">Edit</a>
					{!! Form::open(['method' => 'DELETE','route'=>['products.destroy',$product->id],'style'=>'display:inline']) !!}
         			{!! Form::submit('Delete',['class'=>'btn btn-xs btn-danger']) !!}
          			{!! Form::close() !!}
    			
				</td>
			</tr>
			@endforeach
		</table>
		{!! $products->links() !!}
	</div>



@endsection