@extends('products.master')
@section('content')

	{!! Form::open(['route'=>'products.store','method'=>'POST']) !!}
		@include('products.form')
	{!! Form::close() !!}

@endsection