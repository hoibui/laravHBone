
@extends('products.master')
@section('content')
	<br>
	<h2>Task Detail</h2>
	<br>
	<div class="well well-sm"> <strong>{{$product->name}}</strong></div>
	<div class="well well-sm"> <strong>{{$product->price}}</strong></div>
	
	<div class="form-group">
  		<a class="btn btn-xs btn-primary" href="{{route('products.index')}}">Back</a>
  	</div>
@endsection