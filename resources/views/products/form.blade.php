<br>
<br>

<div class="form-group">
  <label>Name:</label>
  {!! Form::text('name',null) !!}
</div>
<div class="form-group">
  <label>Price:</label>
  {!! Form::text('price',null) !!}
</div> 
<div class="form-group">
  <a class="btn btn-xs btn-success" href="{{route('products.index')}}">Back</a>
  <button type="submit" class="btn btn-xs btn-primary" name="button">Submit</button>
</div>